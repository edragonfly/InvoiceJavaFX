package org.nuts.invoice.service;

import org.nuts.invoice.dao.DetailDao;
import org.nuts.invoice.dao.MasterDao;
import org.nuts.invoice.domain.Detail;
import org.nuts.invoice.domain.Master;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceServiceImpl implements InvoiceService {

    @Autowired
    private MasterDao masterDao;
    @Autowired
    private DetailDao detailDao;

    @Override
    public List<Master> getMasterList() {
        return masterDao.getList();
    }

    @Override
    public List<Detail> getDetailList(int masterId) {
        return detailDao.getList(masterId);
    }
}
