package org.nuts.invoice.service;

import org.nuts.invoice.domain.Detail;
import org.nuts.invoice.domain.Master;

import java.util.List;

public interface InvoiceService {
    List<Master> getMasterList();
    List<Detail> getDetailList(int masterId);
}
