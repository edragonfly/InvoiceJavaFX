package org.nuts.invoice.controller;

import de.felixroske.jfxsupport.FXMLController;
import javafx.beans.Observable;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import lombok.extern.slf4j.Slf4j;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.nuts.invoice.domain.Detail;
import org.nuts.invoice.domain.Master;
import org.nuts.invoice.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ResourceBundle;

@Slf4j
@FXMLController
public class InvoiceController implements Initializable {

    @Autowired
    private InvoiceService invoiceService;

    @FXML
    private TableView<Master> masterTableView;

    @FXML
    private TableColumn<Master, Integer> masterIdTableColumn;
    @FXML
    private TableColumn<Master, LocalDate> dateTableColumn;
    @FXML
    private TableColumn<Master, String> consumerNameTableColumn;
    @FXML
    private TableColumn<Master, String> companyIdTableColumn;
    @FXML
    private TableColumn<Master, String> taxNumberTableColumn;
    @FXML
    private TableColumn<Master, String> addressTableColumn;
    @FXML
    private TableColumn<Master, String> bankTableColumn;


    @FXML
    private TableView<Detail> detailTableView;

    @FXML
    private TableColumn<Detail, Integer> detailMasterIdTableColumn;
    @FXML
    private TableColumn<Detail, String> materialDescriptionTableColumn;
    @FXML
    private TableColumn<Detail, String> invoiceDescriptionTableColumn;
    @FXML
    private TableColumn<Detail, Double> quantity0TableColumn;
    @FXML
    private TableColumn<Detail, Integer> quantity1TableColumn;
    @FXML
    private TableColumn<Detail, Integer> quantity2TableColumn;
    @FXML
    private TableColumn<Detail, Double> amountExcludingTaxTableColumn;
    @FXML
    private TableColumn<Detail, Double> taxTableColumn;
    @FXML
    private TableColumn<Detail, Double> amountIncludingTaxTableColumn;

    @FXML
    private Label message;


    @FXML
    private void exportToXml() {
        //log.info("InvoiceController.exportToXml()");
        //log.info(masterTableView.getSelectionModel().getSelectedItems().toString());

        message.setText("正在检查...");

        ObservableList<Master> selectedItems = masterTableView.getSelectionModel().getSelectedItems();

        long countOfConsumer = selectedItems.stream()
                .map(master -> master.getConsumerName())
                .distinct()
                .count();

        if (countOfConsumer > 1) {
            message.setText("只能选择同一个客户的发票。");
            return;
        }


        try {
            OutputFormat format = OutputFormat.createPrettyPrint();// 创建文件输出的时候，自动缩进的格式
            format.setEncoding("GBK");//设置编码
            XMLWriter writer;

            List<Detail> detailList = detailTableView.getItems();

            int totalRowSize = detailList.size();

            log.info("totalRowSize : {}",String.valueOf(totalRowSize));

            int totalPageSize = (int) Math.ceil(totalRowSize / 8d);

            log.info("totalPageSize : {}",String.valueOf(totalPageSize));


            for (int j = 0; j < totalPageSize; j++) {


                Master master = masterTableView.getSelectionModel().getSelectedItem();
                Document dom = DocumentHelper.createDocument();
                Element kp = dom.addElement("Kp");

                kp.addElement("Version").addText("2.0");

                Element fpxx = kp.addElement("Fpxx");

                fpxx.addElement("Zsl").addText("1");

                Element fpsj = fpxx.addElement("Fpsj");

                Element fp = fpsj.addElement("Fp");

                fp.addElement("Djh").addText(String.valueOf(master.getId()));
                fp.addElement("Gfmc").addText(master.getConsumerName());
                fp.addElement("Gfsh").addText(master.getTaxNumber());
                fp.addElement("Gfyhzh").addText(master.getBank());
                fp.addElement("Gfdzdh").addText(master.getAddress());
                fp.addElement("Bz").addText("");   //master.getComments()
                fp.addElement("Fhr").addText("辛宪平");
                fp.addElement("Skr").addText("常佩芳");
                fp.addElement("Spbmbbh").addText("35.0");
                fp.addElement("Hsbz").addText("0");


                Element spxx = fp.addElement("Spxx");

                int rows = 8;

                if(j+1 == totalPageSize){
                    rows = totalRowSize - 8 * j;
                }



                log.info("rows : {}",String.valueOf(rows));

                int k = 1;

                for (int i = 8 * j; i < 8 * j + rows; i++) {
                    Element sph = spxx.addElement("Sph");

                    sph.addElement("Xh").addText(String.valueOf(k++));
                    sph.addElement("Spmc").addText(detailList.get(i).getInvoiceDescription());
                    sph.addElement("Ggxh").addText(detailList.get(i).getInvoiceSize());
                    sph.addElement("Jldw").addText("平方米");
                    sph.addElement("Sl").addText(String.valueOf(detailList.get(i).getQuantity0()));
                    sph.addElement("Dj").addText(String.valueOf(detailList.get(i).getAmountExcludingTax() / detailList.get(i).getQuantity0()));
                    sph.addElement("Je").addText(String.valueOf(detailList.get(i).getAmountExcludingTax()));
                    sph.addElement("Slv").addText(String.valueOf(0.13));
                    sph.addElement("Se").addText(String.valueOf(detailList.get(i).getTax()));
                    sph.addElement("Spbm").addText(detailList.get(i).getInvoiceGoodsCode());
                    sph.addElement("Qyspbm");
                    sph.addElement("Syyhzcbz").addText("0");
                    sph.addElement("Kce").addText(String.valueOf(0));
                    sph.addElement("Lslbz");
                    sph.addElement("Yhzcsm");
                }

                //String fileName = "d:/it/" + master.getConsumerName() + DateTimeFormatter.ofPattern(" yyyyMMdd HHmmss").format(LocalDateTime.now()) + ".xml";

                String fileName = "d:/it/invoice" + String.valueOf(j+1) + ".xml";

                writer = new XMLWriter(new FileOutputStream(fileName), format);
                writer.write(dom);
                writer.close();


                message.setText("已导出到" + fileName);

            }

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }


    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        log.info("InvoiceController.initialize()");

        ObservableList<Master> masterList = FXCollections.observableList(invoiceService.getMasterList());

        /*
        masterIdTableColumn.setCellValueFactory((TableColumn.CellDataFeatures<Master, Number> cdf) -> {
            Master master = cdf.getValue();
            return new SimpleIntegerProperty(master.getId());
            //return new SimpleStringProperty(String.valueOf(master.getId()));
            //return new SimpleStringProperty(String.valueOf(master.getId()));

        });

         */

        //表头列

        companyIdTableColumn.setCellValueFactory(new PropertyValueFactory<>("companyId"));
        masterIdTableColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        dateTableColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
        consumerNameTableColumn.setCellValueFactory(new PropertyValueFactory<>("consumerName"));
        taxNumberTableColumn.setCellValueFactory(new PropertyValueFactory<>("taxNumber"));
        addressTableColumn.setCellValueFactory(new PropertyValueFactory<>("address"));
        bankTableColumn.setCellValueFactory(new PropertyValueFactory<>("bank"));


        //表体列
        detailMasterIdTableColumn.setCellValueFactory(new PropertyValueFactory<>("masterId"));
        materialDescriptionTableColumn.setCellValueFactory(new PropertyValueFactory<>("materialDescription"));
        invoiceDescriptionTableColumn.setCellValueFactory(new PropertyValueFactory<>("invoiceDescription"));
        quantity0TableColumn.setCellValueFactory(new PropertyValueFactory<>("quantity0"));
        quantity1TableColumn.setCellValueFactory(new PropertyValueFactory<>("quantity1"));
        quantity2TableColumn.setCellValueFactory(new PropertyValueFactory<>("quantity2"));
        amountExcludingTaxTableColumn.setCellValueFactory(new PropertyValueFactory<>("amountExcludingTax"));
        taxTableColumn.setCellValueFactory(new PropertyValueFactory<>("tax"));
        amountIncludingTaxTableColumn.setCellValueFactory(new PropertyValueFactory<>("amountIncludingTax"));


        masterTableView.setItems(masterList);

        TableView.TableViewSelectionModel<Master> selectionModel = masterTableView.getSelectionModel();

        selectionModel.setSelectionMode(SelectionMode.MULTIPLE);

        ObservableList<Master> selectedItems = selectionModel.getSelectedItems();
        selectedItems.addListener((Observable observable) -> {

            detailTableView.getItems().clear();

            selectedItems.stream().forEach((master) -> {
                //log.info(invoiceService.getDetailList(master.getId()).toString());
                detailTableView.getItems().addAll(invoiceService.getDetailList(master.getId()));
            });
        });

    }

}
