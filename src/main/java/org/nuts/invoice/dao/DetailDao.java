package org.nuts.invoice.dao;


import org.apache.ibatis.annotations.Mapper;
import org.nuts.invoice.domain.Detail;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface DetailDao {
    List<Detail> getList(int masterId);
}
