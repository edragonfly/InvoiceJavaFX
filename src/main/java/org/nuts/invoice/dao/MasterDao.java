package org.nuts.invoice.dao;

import org.apache.ibatis.annotations.Mapper;
import org.nuts.invoice.domain.Master;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface MasterDao {
    List<Master> getList();
}
