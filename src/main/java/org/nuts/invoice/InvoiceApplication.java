package org.nuts.invoice;


import de.felixroske.jfxsupport.AbstractJavaFxApplicationSupport;
import org.mybatis.spring.annotation.MapperScan;
import org.nuts.invoice.service.InvoiceService;
import org.nuts.invoice.view.InvoiceFxmlView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
public class InvoiceApplication extends AbstractJavaFxApplicationSupport {
    public static void main(String[] args) {
        //SpringApplication.run(InvoiceApplication.class, args);
        launch(InvoiceApplication.class, InvoiceFxmlView.class,args);
    }
}
