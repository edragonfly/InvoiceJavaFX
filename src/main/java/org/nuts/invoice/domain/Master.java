package org.nuts.invoice.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
public class Master {
    private int id;
    private String consumerName;
    private LocalDate date;
    private String companyId;
    private String taxNumber;
    private String address;
    private String bank;
}
