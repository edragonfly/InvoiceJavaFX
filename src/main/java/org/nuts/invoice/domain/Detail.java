package org.nuts.invoice.domain;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Detail {
    private int masterId;
    private String materialDescription;
    private String invoiceDescription;
    private String invoiceSize;
    private double quantity0;
    private int quantity1;
    private int quantity2;
    private double amountExcludingTax;
    private double tax;
    private double amountIncludingTax;
    private String invoiceGoodsCode;
}
