package org.nuts.invoice.util;

import lombok.extern.slf4j.Slf4j;
import org.nuts.invoice.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

//@Configuration
//@EnableScheduling
@Slf4j
public class ScheduleTask {

    @Autowired
    private InvoiceService invoiceService;

    @Scheduled(fixedDelay = 10000)
    private void configureTasks() {
        System.out.println("我是一个定时任务");
        log.info(invoiceService.getMasterList().toString());
    }
}
