package org.nuts.invoice.view;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

@FXMLView(value = "/template/invoice.fxml")
public class InvoiceFxmlView extends AbstractFxmlView {
}
